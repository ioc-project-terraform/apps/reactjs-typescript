const formatDate = (date: string): string => {
    const dateFormatted = new Date(date)
    const options : Intl.DateTimeFormatOptions= { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

    return dateFormatted.toLocaleString('pt-BR', options)
  };
  
  export default formatDate;