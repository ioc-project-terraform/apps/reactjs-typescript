import React, { useMemo, useState, useEffect } from "react";
import ContentHeader from "../../Components/ContentHeader";
import HistoryCard from "../../Components/HistoryCard";
import SelectInput from "../../Components/SelectInput";
import expenses from "../../repositories/expenses";
import gains from "../../repositories/gains";
import emojis from "../../Utils/emojis";
import formatCurrency from "../../Utils/format";
import formatDate from "../../Utils/formateDate";
import { Container, Content, Filters } from "./style";



interface IRouteParams {
  match: {
    params: {
      type: string
    }
  }

}

interface IData {
  description: string;
  amount: string;
  frequency: string;
  date: string;
  tagColor: string
}

const List: React.FC<IRouteParams> = ({ match }) => {

  const monthOptions: Intl.DateTimeFormatOptions = { month: 'long' };
  const [data, setData] = useState<IData[]>([])
  const [monthSelected, setMonthSelected] = useState<string>(String(new Date().toLocaleString('pt-BR', monthOptions)))
  const [yearSelected, setYearSelected] = useState<string>(String(new Date().getFullYear()))
  const [frequency, setFrequency] = useState<string[]>(['recorrente', 'eventual'])

  const { type } = match.params;

  const value = useMemo(() => {

    return type === 'entry-balance' ?
      { title: 'Incomes', lineColor: '#F7931B', data: gains } : { title: 'Outcomes', lineColor: '#E44C4E', data: expenses }

  }, [type])


  const months = useMemo(() => {

    let uniqueMonths: string[] = []



    value.data.forEach(d => {
      const date = new Date(d.date);
      const month = date.toLocaleString('pt-BR', monthOptions)

      if (!uniqueMonths.includes(month)) {
        uniqueMonths.push(month)
      }
    })

    return uniqueMonths.map(month => {
      return {
        value: month,
        label: month
      }
    })

  }, [value.data, monthOptions])


  const years = useMemo(() => {

    let uniqueYears: number[] = []
    value.data.forEach(d => {

      const date = new Date(d.date);
      const year = date.getFullYear()


      if (!uniqueYears.includes(year)) {
        uniqueYears.push(year)
      }
    })
    return uniqueYears.map(year => {
      return {
        value: year,
        label: year
      }
    })
  }, [value.data])



  useEffect(() => {
    const filteredDate = value.data.filter(i => {
      const date = new Date(i.date);
      const month = String(date.toLocaleString('pt-BR', monthOptions))
      const year = String(date.getFullYear())
      return month === monthSelected && year === yearSelected && frequency.includes(i.frequency)

    })

    const response = filteredDate.map(i => {
      return {
        description: i.description,
        amount: i.amount,
        frequency: i.frequency,
        date: i.date,
        tagColor: i.frequency === 'recorrente' ? '#4E41F0' : '#E44C4E'
      }
    })

    setData(response)
  }, [monthSelected, yearSelected, value.data, frequency])

  const handleFrequencyClick = (f: string) => {
    const alreadySelected = frequency.findIndex(v => v === f)

    if (alreadySelected > -1) {
      const filtered = frequency.filter(x => x === f)
      setFrequency(filtered)
    } else {
      setFrequency((prev => [...prev, f]))
    }
  }

  return (
    <Container>
      <ContentHeader title='Expenses' lineColor='#E44C4E'>
        <SelectInput options={months} onChange={e => setMonthSelected(e.target.value)} current={monthSelected} ></SelectInput>
        <SelectInput options={years} onChange={e => setYearSelected(e.target.value)} current={yearSelected}></SelectInput>
      </ContentHeader>

      <Filters>
        <button className={`tag-filter tag-filter-recurring ${frequency.includes('recorrente') && 'tag-activated'}`} onClick={() => handleFrequencyClick('recorrente')}>Recurring</button>
        <button className={`tag-filter tag-filter-occasionally ${frequency.includes('eventual') && 'tag-activated'}`} onClick={() => handleFrequencyClick('eventual')}>Occasionally </button>
      </Filters>
      <Content>
        {
          data.map((i, index) => {
            return <HistoryCard
              key={index}
              cardColor='#313862'
              title={i.description}
              tagColor={i.tagColor}
              subtitle={formatDate(i.date)}
              amount={formatCurrency(Number(i.amount))}>

            </HistoryCard>
          })
        }


      </Content>
    </Container>
  );
};

export default List;
