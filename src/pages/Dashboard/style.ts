import styled from 'styled-components';

export const Container = styled.div``
export const Content = styled.div`
    display: flex;
    justify-content: space-between;
    gap: 10px;
    flex-wrap: wrap;
`