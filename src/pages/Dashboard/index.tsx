import React, { useMemo, useState } from "react";

import ContentHeader from "../../Components/ContentHeader";
import HistoryBox from "../../Components/HistoryBox";
import MessageBox from "../../Components/MessageBox";
import PieChartComponent from "../../Components/PieChart";
import SelectInput from "../../Components/SelectInput";
import WalletBox from "../../Components/WalletBox";
import expenses from "../../repositories/expenses";
import gains from "../../repositories/gains";
import { Container, Content } from "./style";

const Dashboard: React.FC = () => {

  const monthOptions: Intl.DateTimeFormatOptions = { month: 'long' };

  const [monthSelected, setMonthSelected] = useState<number>(new Date().getMonth() + 1)
  const [yearSelected, setYearSelected] = useState<number>(new Date().getFullYear())

  const totalExpenses = useMemo(() => {
    let total: number = 0;
    expenses.forEach(e => {
      const date = new Date(e.date)
      const month = date.getMonth() + 1
      const year = date.getFullYear()
      if (month === monthSelected && year === yearSelected) {
        total += Number(e.amount)
      }
    })

    return total;

  }, [monthSelected, yearSelected])

  const totalIncome = useMemo(() => {
    let total: number = 0;
    gains.forEach(e => {
      const date = new Date(e.date)
      const month = date.getMonth() + 1
      const year = date.getFullYear()
      if (month === monthSelected && year === yearSelected) {
        total += Number(e.amount)
      }
    })

    return total;

  }, [monthSelected, yearSelected])

  const totalBalance = useMemo(() => {
    return totalIncome - totalExpenses

  }, [totalExpenses, totalIncome])

  const message = useMemo(() => {

    if (totalBalance < 0) {
      return {
        title: 'Could do better',
        description: 'Your balance is negative!',
        foot: 'You need to spend less and save more',
        icon: 'sad'
      }
    } else {
      return {
        title: 'Great work',
        description: 'Your balance is positive!',
        foot: 'Keep going - Your financial future is SAVE!',
        icon: 'happy'
      }
    }

  }, [totalBalance])

  const relationExpensesGains = useMemo(() => {

    const total = totalIncome + totalExpenses

    const percentGains = (totalIncome / total) * 100;
    const percentExpeses = (totalExpenses / total) * 100;

    const data = [
      {
        name: 'Income',
        value: percentGains,
        percent: Number(percentGains.toFixed(1)),
        color: '#F7931B'
      },
      {
        name: 'Expenses',
        value: percentExpeses,
        percent: Number(percentExpeses.toFixed(1)),
        color: '#E44C4E'
      }
    ]

    return data;

  }, [totalIncome, totalExpenses])

  const historyData = useMemo(() => {
    const listOfMonths = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"]

    return listOfMonths.map((_, month) => {

      let amountEntry = 0;
      gains.forEach(gain => {
        const date = new Date(gain.date)
        const gainMonth = date.getMonth()
        const gainYear = date.getFullYear()

        if (gainMonth === month && gainYear === yearSelected) {
          amountEntry += Number(gain.amount)
        }
      })

      let amountOutput = 0;
      expenses.forEach(expense => {
        const date = new Date(expense.date)
        const expenseMonth = date.getMonth()
        const expenseYear = date.getFullYear()

        if (expenseMonth === month && expenseYear === yearSelected) {
          amountOutput += Number(expense.amount)
        }
      })


      return {
        monthNumber: month,
        month: listOfMonths[month].substr(0, 3),
        amountEntry,
        amountOutput
      }



    }).filter( item => {
      const date = new Date()
      const currentMonth = date.getMonth()
      const currentYear = date.getFullYear()

      return (yearSelected === currentYear && item.monthNumber <= currentMonth) || (yearSelected < currentYear)
    })



  }, [yearSelected])

  const months = useMemo(() => {

    let uniqueMonths: { label: string, value: number }[] = []

    const mergedArray = [...gains, ...expenses].forEach(d => {
      const date = new Date(d.date);
      const month = date.toLocaleString('pt-BR', monthOptions)

      if (!uniqueMonths.some(e => e.label === month)) {
        uniqueMonths.push({ label: month, value: date.getMonth() + 1 })
      }

    })

    return uniqueMonths.sort((n1, n2) => n1.value - n2.value)

  }, [])

  const years = useMemo(() => {

    let uniqueYears: number[] = []

    const mergedArray = [...gains, ...expenses].forEach(d => {

      const date = new Date(d.date);
      const year = date.getFullYear()


      if (!uniqueYears.includes(year)) {
        uniqueYears.push(year)
      }
    })
    return uniqueYears.map(year => {
      return {
        value: year,
        label: year
      }
    })
  }, [])

  return (
    <Container>
      <ContentHeader title='Dashboard' lineColor='#e44c4e'>
        <SelectInput options={months} onChange={e => setMonthSelected(Number(e.target.value))} current={String(monthSelected)} ></SelectInput>
        <SelectInput options={years} onChange={e => setYearSelected(Number(e.target.value))} current={String(yearSelected)}></SelectInput>
      </ContentHeader>
      <Content>
        <WalletBox
          color='#4e41f0'
          amount={totalBalance}
          title='Balance'
          footerlabel='Update based on the flow of incomes and expenses'
          icon='dolar' />
        <WalletBox
          color='#F7931B'
          amount={totalIncome}
          title='Income'
          footerlabel='Update based on the flow of incomes and expenses'
          icon='arrowUp' />
        <WalletBox
          color='#e44c4e'
          amount={totalExpenses}
          title='Expenses'
          footerlabel='Update based on the flow of incomes and expenses'
          icon='arrowDown' />


        <MessageBox
          title={message.title}
          icon={message.icon}
          description={message.description}
          footertext={message.foot} />

        <PieChartComponent data={relationExpensesGains} />

        <HistoryBox
          data={historyData}
          lineColorAmountEntry='#F7931B'
          lineColorAmountOutput='#e44c4e'
        />

      </Content>

    </Container>
  );
};

export default Dashboard;
