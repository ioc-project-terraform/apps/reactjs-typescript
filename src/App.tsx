import React from "react";
import GlobalStyles from "./Styles/GlobalStyle";

import { ThemeProvider } from 'styled-components'

import dark from "./Styles/Themes/dark";
import Routes from "./routes";

const App: React.FC = () => {
  return (
    <ThemeProvider theme={dark}>
      <GlobalStyles />
      <Routes></Routes>
    </ThemeProvider>
  );
};

export default App;
