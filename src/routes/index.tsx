import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import AppRoutes from './app.routes';

export interface AppProps {

}

const Routes: React.SFC<AppProps> = () => {
    return (
        <BrowserRouter>
            <AppRoutes />
        </BrowserRouter>
    );
}

export default Routes;
