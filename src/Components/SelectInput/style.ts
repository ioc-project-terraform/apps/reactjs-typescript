import styled from "styled-components";

export const Container = styled.div`

  > select {
    padding: 7px 10px;
    width: 140px;
    
    border: 0px;
    outline: none;
    margin-left: 8px;
    
  }
`;
