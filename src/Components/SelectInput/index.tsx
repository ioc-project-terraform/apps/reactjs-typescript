import * as React from 'react';
import { Container } from './style';

export interface ISelectInputProps {
    options: {
        value: string | number;
        label: string | number;

    }[],
    onChange(event: React.ChangeEvent<HTMLSelectElement>): void | undefined;
    current?: string;
}

const SelectInput: React.SFC<ISelectInputProps> = ({ options, onChange, current }) => {
    return (
        <Container>
            <select name="" id="" onChange={onChange} value={current}>
                {options.map(o => {
                    return <option key={o.value} value={o.value}>{o.label} </option>
                })}
            </select>
        </Container>);
}

export default SelectInput;