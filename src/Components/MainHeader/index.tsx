import React, { useMemo } from "react";
import emojis from "../../Utils/emojis";
import Toggle from "../Toggle";

import { Container, Profile, UserName, Welcome } from './styles'

const MainHeader: React.FC = () => {

  const emoji = useMemo(() => {
    const index = Math.floor(Math.random() * emojis.length)
    return emojis[index];
  }, [])
  return (
    <>
      <Container>
       <Toggle></Toggle>
        <Profile>
          <Welcome>Ola, {emoji}</Welcome>
          <UserName>Robson Carvalho</UserName>

        </Profile>
      </Container>
    </>
  );
};

export default MainHeader;
