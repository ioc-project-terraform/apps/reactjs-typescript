import * as React from 'react';
import { Container, TitleContainer, Controllers } from './style';

export interface IContentHeaderProps {
    title: string,
    lineColor: string,
    children: React.ReactNode

}

const ContentHeader: React.SFC<IContentHeaderProps> = ({ title, lineColor, children }: IContentHeaderProps) => {
    return (

        <Container>
            <TitleContainer lineColor={lineColor}>
                <h1>{title}</h1>
            </TitleContainer>
            <Controllers>
                {children}
            </Controllers>
        </Container>);
}

export default ContentHeader;