import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 25px;
  width: 100%;
`;

export const Header = styled.div``;



type ITitleContainerProps = {
  lineColor: string
}

export const TitleContainer = styled.div<ITitleContainerProps>`

h1 {
    &:after {
      display: block;
      content: "";
      position: relative;
      border-top: 0.3rem solid ${props => props.lineColor};
      width: 4rem;
      top: 20px;
      transform: translateY(-1rem);
    }
  }

`


  
export const Controllers = styled.div`
  display: flex;
  
`;
