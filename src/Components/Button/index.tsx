import React from "react";
import { StyledButton } from "./style";

type Props = {
  disabled: boolean;
};

const ButtonComponent = ({ disabled }: Props) => {
  console.log(disabled)
  return <StyledButton disabled={disabled}>Click me</StyledButton>;
};

export default ButtonComponent;
