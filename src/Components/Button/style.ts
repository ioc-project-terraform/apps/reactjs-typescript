import tw, { styled, theme, css } from "twin.macro";

type Props = {
  disabled: boolean
}
export const StyledButton = styled.button(({disabled}:Props) => [
  tw`py-3 px-8 uppercase rounded border border-primary hover:bg-primary duration-100`,

  css`
      & {
          background-color: ${theme`colors.whiteAlt`};
      }

      &:hover {
          font-size: 2rem;
      }
  `,

disabled && tw`background-color: text-red-700`, // new
 
]);
