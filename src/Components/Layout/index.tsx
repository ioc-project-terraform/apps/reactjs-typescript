import React from "react";

import { Container } from './styles'
import Content from './../Content/index';
import MainHeader from './../MainHeader/index';
import Aside from "../Aside";



const Layout: React.FC = ({children}) => {
  return (
      <Container>
        <MainHeader />
        <Aside />
        <Content> 
          {children}
        </Content>
      </Container>
  );
};

export default Layout;
