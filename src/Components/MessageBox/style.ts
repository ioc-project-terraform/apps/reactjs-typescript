import  styled  from 'styled-components';

interface IContainerProps {
   
}
export const Container = styled.div<IContainerProps>`
    width: 48%;
    background-color: ${props => props.theme.colors.tertiary};
    padding: 30px 20px;
    border-radius: 10px;
    height: 260px;
    
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    
    margin: 10px 0;
    
    header {
        transition: all .3s;
    }
    
    header svg {
        width: 35px;
        margin-left: 5px
    }

    header p {
        font-size: 16px;
    }
`
