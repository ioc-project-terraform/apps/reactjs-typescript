import React from 'react'
import { Container } from './style';


export interface IMessageBoxProps {
    title: string;
    description: string;
    footertext: string;
    icon: string;
}

const MessageBox: React.SFC<IMessageBoxProps> = ({ title, description, footertext, icon }) => {

    return (
        <Container>

            <header>
                <h1>
                    {title}
                    {
                        icon === 'happy' ?
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                            :
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.172 16.172a4 4 0 015.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                    }

                </h1>

                <p>{description}</p>
            </header>
            <footer>
                <span>{footertext}</span>
            </footer>

        </Container>);
}

export default MessageBox;