import styled from "styled-components";

export const Container = styled.div`
  grid-area: AS;
  color: ${(props) => props.theme.colors.white};
  background-color: ${(props) => props.theme.colors.tertiary};
  padding-left: 20px;
  border-right: 1px solid ${(props) => props.theme.colors.gray};
`;

export const Header = styled.header`
  display: flex;
  align-items: center;
  margin: 20px 0px;
`;

export const Logo = styled.div`
  color: ${(props) => props.theme.colors.info};
`;
export const Title = styled.span`
  margin-left: 10px;
  font-size: 1.5rem;
  font-weight: 700;
  top: -2px;
  position: relative;
`;

export const MenuContainer = styled.nav`
  display: flex;
  flex-direction: column;
  margin-top: 25px;
`;
export const MenuItemLink = styled.a`
  color: ${(props) => props.theme.colors.info};
  text-decoration: none;
  margin: 8px 0px;
  transition: opacity 0.3s;
  &:hover {
    opacity: 0.7;
  }
  display: flex;
  align-items: center;
`;
