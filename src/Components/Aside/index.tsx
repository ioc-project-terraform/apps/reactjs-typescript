import React from "react";
import tw from 'twin.macro'

import { Container, Header, Logo, MenuContainer, MenuItemLink, Title } from './styles'

const Aside: React.FC = () => {
  return (
    <>
      <Container>
        <Header>
          <Logo >

            <svg xmlns="http://www.w3.org/2000/svg" css={tw`h-8 w-8`} fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>



          </Logo>
          <Title>My Wallet</Title>
        </Header>
        <MenuContainer>
          <MenuItemLink href="/dashboard" css={tw`space-x-2`}>
            <svg xmlns="http://www.w3.org/2000/svg" css={tw`h-6 w-6`} fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 8v8m-4-5v5m-4-2v2m-2 4h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
            </svg>
            <span>Dashboard</span>
          </MenuItemLink>
          <MenuItemLink href="/list/entry-balance" css={tw`space-x-2`}>
            <svg xmlns="http://www.w3.org/2000/svg" css={tw`h-6 w-6`} fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 11l3-3m0 0l3 3m-3-3v8m0-13a9 9 0 110 18 9 9 0 010-18z" />
            </svg>
            <span> Incomes</span>

          </MenuItemLink>
          <MenuItemLink href="/list/exit-balance" css={tw`space-x-2`}>

            <svg xmlns="http://www.w3.org/2000/svg" css={tw`h-6 w-6`} fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 13l-3 3m0 0l-3-3m3 3V8m0 13a9 9 0 110-18 9 9 0 010 18z" />
            </svg>
            <span> Expenses</span>

          </MenuItemLink>
          <MenuItemLink href="#" css={tw`space-x-2`}>
            <svg xmlns="http://www.w3.org/2000/svg" css={tw`h-6 w-6`} fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
            </svg>
            <span>  Logout</span>
          </MenuItemLink>
        </MenuContainer>

      </Container>
    </>
  );
};

export default Aside;
