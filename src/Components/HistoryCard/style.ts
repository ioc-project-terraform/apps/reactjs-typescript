import styled from "styled-components";


interface ITContainerProps {
  color: string;
}
export const Container = styled.li<ITContainerProps>`
    background-color: ${props => props.color};
    list-style: none;
    border-radius: 5px;
    margin: 10px 0px;
    padding: 12px 10px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    cursor: pointer;
    transition: all .3s;

    &:hover{
      opacity: .7;
      transform: translateX(10px)
    }
    position: relative;

    > div {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      padding-left: 10px;
    }
`;

interface ITagProps {
  color: string;
  
}
export const Tag = styled.div<ITagProps>`
  position: absolute;
  width: 8px;
  height: 60%;
  left: 0;
  background-color: ${(props) => (props.color ? props.color : "blue")};;
`;
