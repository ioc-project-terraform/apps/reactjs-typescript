import React from 'react'
import { Container } from './style';
import tw from 'twin.macro';
import formatCurrency from '../../Utils/format';

import CountUp from 'react-countup'

export interface IWalletBoxProps {
    title?: string;
    amount: number;
    footerlabel?: string;
    icon?: 'dolar' | 'arrowUp' | 'arrowDown';
    color: string
}

const WalletBox: React.SFC<IWalletBoxProps> = ({ title, amount, icon, footerlabel, color }) => {


    const iconSelected = () => {

        switch (icon) {
            case 'dolar':
                return (
                    <svg xmlns="http://www.w3.org/2000/svg"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                )
            case 'arrowDown':
                return (
                    <svg xmlns="http://www.w3.org/2000/svg"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 13l-3 3m0 0l-3-3m3 3V8m0 13a9 9 0 110-18 9 9 0 010 18z" />
                    </svg>
                )
            case 'arrowUp':
                return (
                    <svg xmlns="http://www.w3.org/2000/svg"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 11l3-3m0 0l3 3m-3-3v8m0-13a9 9 0 110 18 9 9 0 010-18z" />
                    </svg>
                )
            default:
                break;
        }
    }
    return (
        <Container color={color}>
            <span>{title}</span>
            <h1>
                
                <CountUp 
                        end={amount}
                        prefix= {'R$'}
                        separator="."
                        decimal=","
                        decimals={2}
                        
                        />

          
            </h1>
            <small>{footerlabel}</small>
            {icon && iconSelected()}
        </Container>);
}

export default WalletBox;