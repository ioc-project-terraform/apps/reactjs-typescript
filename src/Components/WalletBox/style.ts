import  styled  from 'styled-components';

interface IContainerProps {
    color: string
}
export const Container = styled.div<IContainerProps>`
    background-color: ${props => props.color};
    width: 32%;
    padding: 10px 20px;
    border-radius: 10px;
    margin: 10px 0;
    height: 120px;

    position: relative;
    overflow: hidden;

    > svg  {
        position: absolute;
        right: -30px;
        top: -10px;
        height: 130%;
        opacity: .2;
    }

    > span {
        font-size: 16px;
        font-weight: 500;
    }
    
    > small {
        font-size: 12px;
        position: absolute;
        bottom: 15px;
    }
    
`
