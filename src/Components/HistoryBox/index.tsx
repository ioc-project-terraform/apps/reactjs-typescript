import * as React from 'react';
import { ChartContainer, Container, Header, Legend, LegendContainer } from './style';

import {
    ResponsiveContainer,
    LineChart,
    Line,
    XAxis,
    CartesianGrid,
    Tooltip
} from 'recharts'
import formatCurrency from '../../Utils/format';

export interface HistoryBoxProps {
    data: {
        month: string;
        amountEntry: number;
        amountOutput: number;
    }[],
    lineColorAmountEntry: string;
    lineColorAmountOutput: string;
}

const HistoryBox: React.SFC<HistoryBoxProps> = ({ data, lineColorAmountEntry, lineColorAmountOutput }) => (

    <Container>
        <Header>
            <h2>History Box</h2>
            <LegendContainer>
                <Legend color={lineColorAmountOutput}>
                    <div></div>
                    <span>Expenses</span>
                </Legend>

                <Legend color={lineColorAmountEntry}>
                    <div></div>
                    <span>Income</span>
                </Legend>

            </LegendContainer>
        </Header>
        <ChartContainer>
            <ResponsiveContainer>
                <LineChart
                    data={data}
                    margin={{ top: 5, right: 20, left: 20, bottom: 5 }}>
                    <CartesianGrid
                        strokeDasharray="3 3"
                        stroke="#cecece" />
                    <XAxis
                        dataKey="month"
                        stroke="#cecece" />
                    <Tooltip formatter={(value: any) => formatCurrency(Number(value))} />
                    <Line
                        type="monotone"
                        dataKey="amountOutput"
                        name="Expenses"
                        stroke={lineColorAmountOutput}
                        strokeWidth={5}
                        dot={{ r: 5 }}
                        activeDot={{ r: 8 }} />
                    <Line
                        type="monotone"
                        dataKey="amountEntry"
                        name="Income"
                        stroke={lineColorAmountEntry}
                        strokeWidth={5}
                        dot={{ r: 5 }}
                        activeDot={{ r: 8 }} />
                </LineChart>
            </ResponsiveContainer>
        </ChartContainer>
    </Container>

)

export default HistoryBox;