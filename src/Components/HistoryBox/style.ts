import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  background-color: ${(props) => props.theme.colors.tertiary};

  padding: 30px 20px;

  border-radius: 10px;
`;

export const ChartContainer = styled.div`
  height: 300px;
`;

export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;

  > h2 {
    margin-bottom: 20px;
    padding-left: 18px;
  }
`;
export const LegendContainer = styled.ul`
  list-style: none;
  display: flex;
  padding-right: 18px;
`;

interface ILegendProps {
  color: string
}
export const Legend = styled.li<ILegendProps>`
  display: flex;
  align-items: center;
  font-size: 14px;
  margin-left: 7px;
  margin-bottom: 7px;
  

  > div {
    background-color: ${(props) => props.color};
    width: 40px;
    height: 40px;
    padding: 5px;
    border-radius: 3px;
    font-size: 12px;
    line-height: 30px;
    text-align: center;
  }

  > span {
    margin-left: 5px;
  }
`;
