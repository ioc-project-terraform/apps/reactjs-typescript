import * as React from 'react';
import { Container, Legend, LegendContainer, SideLeft, SideRight } from './style';

import { PieChart, Pie, Cell, ResponsiveContainer } from 'recharts'

export interface PieChartProps {
    data:
    {
        name: string;
        value: number,
        percent: number,
        color: string,
    }[]

}

const PieChartComponent: React.SFC<PieChartProps> = ({ data }) => {
    return (
        <Container>
            <SideLeft>
                <h2>Relation</h2>
                <LegendContainer>
                    {
                        data.map(l => {
                            return (
                                <Legend key={l.name}
                                    color={l.color}>
                                    <div>{l.percent}%</div>
                                    <span>{l.name}</span>
                                </Legend>
                            )
                        })
                    }


                </LegendContainer>
            </SideLeft>
            <SideRight>
                <ResponsiveContainer>
                    <PieChart>
                        <Pie
                            data={data}
                            dataKey="percent"
                        >
                            {
                                data.map((i) => (

                                    <Cell
                                        key={i.name}
                                        fill={i.color} />)
                                )
                            }

                        </Pie>
                    </PieChart>
                </ResponsiveContainer>
            </SideRight>

        </Container>);
}

export default PieChartComponent;