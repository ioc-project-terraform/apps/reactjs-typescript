import styled from "styled-components";

interface IlegendProps {
  color: string;
}
export const Container = styled.div`
  width: 50%;
  height: 260px;
  margin: 10px 0;

  background-color: ${(props) => props.theme.colors.tertiary};

  border-radius: 10px;
  display: flex;
  padding: 30px 20px;
`;

export const SideLeft = styled.div`
  > h2 {
    margin-bottom: 20px;
  }
`;
export const SideRight = styled.main`

  display: flex;
  flex: 1;
  justify-content: center;
  
 
`;
export const LegendContainer = styled.ul`
  list-style: none;
`;
export const Legend = styled.li<IlegendProps>`
  display: flex;
  align-items: center;
  font-size: 14px;

  margin-bottom: 7px;
  
  > div {
    background-color: ${(props) => props.color};
    width: 40px;
    height: 40px;
    padding: 5px;
    border-radius: 3px;
    font-size: 12px;
    line-height: 30px;
    text-align: center;
  }

  > span {
      margin-left: 5px;
  }


`;
